package dat251.cohesion.example02;

public class CoffeeCup {
	
    private int innerCoffee;
    
    
    public void add(int amount) {
        innerCoffee += amount;
    }
    
    
    public int remove(boolean all, int amount) {
        int returnValue = 0;
        if (all) {
            // set innerCoffee to 0
            // ignore parameter amount
            int allCoffee = innerCoffee;
            innerCoffee = 0;
            returnValue = allCoffee; // return all coffee
        }
        else {
            // remove the amount of coffee passed as amount
            int sip = amount;
            if (innerCoffee < amount) {
                sip = innerCoffee;
            }
            innerCoffee -= sip;
            returnValue = sip; // return removed amount
        }
       return returnValue;
    }
    

	public static void main(String[] args) {
		CoffeeCup cup = new CoffeeCup();
        cup.add(355);
        int mlCoffee = cup.remove(false, 25);
        mlCoffee += cup.remove(true, -1);
        System.out.println("Ml of coffee: " + mlCoffee);
	}

}
