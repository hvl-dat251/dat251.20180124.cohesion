package dat251.cohesion.example03;

public class CoffeeCup {
	
    private int innerCoffee;

    
    public void add(int amount) {
        innerCoffee += amount;
    }
    
    
    public int releaseOneSip(int sipSize) {
        int sip = sipSize;
        if (innerCoffee < sipSize) {
            sip = innerCoffee;
        }
        innerCoffee -= sip;
        return sip;
    }
    
    
    public int spillEntireContents() {
        int all = innerCoffee;
        innerCoffee = 0;
        return all;
    }
    

	public static void main(String[] args) {
		CoffeeCup cup = new CoffeeCup();
        cup.add(355);
        int mlCoffee = cup.releaseOneSip(25);
        mlCoffee += cup.spillEntireContents();
        System.out.println("Ml of coffee: " + mlCoffee);
    }

}
