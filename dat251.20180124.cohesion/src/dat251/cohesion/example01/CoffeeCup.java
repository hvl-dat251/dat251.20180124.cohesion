package dat251.cohesion.example01;

public class CoffeeCup {
	
    public final static int ADD = 0;
    public final static int RELEASE_SIP = 1;
    public final static int SPILL = 2;
    
    private int innerCoffee;
    
    
    public int modify(int action, int amount) {
        int returnValue = 0;
        switch (action) {
        case ADD:
            // add amount of coffee
            innerCoffee += amount;
            // return zero, even though that is meaningless
            break;
        case RELEASE_SIP:
            // remove the amount of coffee passed as amount
            int sip = amount;
            if (innerCoffee < amount) {
                sip = innerCoffee;
            }
            innerCoffee -= sip;
            // return removed amount
            returnValue = sip;
            break;
        case SPILL:
            // set innerCoffee to 0
            // ignore parameter amount
            int all = innerCoffee;
            innerCoffee = 0;
            // return all coffee
            returnValue = all;
        default:
            // Here should throw an exception, because they
            // passed an invalid command down in action
            break;
        }
        return returnValue;
    }
    

	public static void main(String[] args) {
        CoffeeCup cup = new CoffeeCup();
        // ignore bogus return value of modify() in ADD case
        cup.modify(CoffeeCup.ADD, 355);
        int mlCoffee = cup.modify(CoffeeCup.RELEASE_SIP, 25);
        // 2nd parameter is unused in SPILL case
        mlCoffee += cup.modify(CoffeeCup.SPILL, 0);
        System.out.println("Ml of coffee: " + mlCoffee);
	}

}
